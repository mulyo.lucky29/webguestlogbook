# Guest Log Book
##

Guest Log Book adalah aplikasi mobile yang dapat di akses via device milik tamu baik di Android maupun di IOS platform.
Tujuannya utama adalah untuk mendigitalisasi buku tamu yang saat ini masih manual kertas/buku.
aplikasi ini berbasis web, sehingga tidak perlu di install terlebih dahulu.
Tamu yang menggunakan aplikasi ini akan di arahkan untuk mengakses barcode URL yang di sediakan di meja receptionis di masing-masing site. 

## Source File
Source file bisa akses url berikut
[https://gitlab.com/mulyo.lucky29/flutterguestlogbook](https://gitlab.com/mulyo.lucky29/flutterguestlogbook)

## Demo 
untuk demo aplikasi bisa akses url berikut
[https://webggguestlogbook.herokuapp.com/#/demo](https://webggguestlogbook.herokuapp.com/#/demo)

## Flow Aplikasi 
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestlogbook/flow.png?raw=true)

![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestlogbook/guestlogbook_flow.png?raw=true)

## ID Exchange 
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestlogbook/guestbook_id_exchange.JPG?raw=true)

## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
